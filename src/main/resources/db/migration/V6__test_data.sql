INSERT INTO users (username, password, enabled)
VALUES ('user1', '$2a$12$2YtF.UXeAHoIn3CZrggUYOGHlgctnDjIuBdIcKn7gXigK3fHEQV5u', true), -- password1
       ('user2', '$2a$12$sdjtxrL9qFPFwEDgXllvyOXtDpKijBxPUxFZ/c8XzFN0GCkA4upPS', true), -- password2
       ('user3', '$2a$12$hGlSTSVJjBN.b3noAPV9wu7r5vSzvdd70i9mo1xvnth.g.ubso7jO', true); -- password3

INSERT INTO authorities(username, authority)
VALUES ('user1', 'ROLE_ADMIN'),
       ('user2', 'ROLE_USER'),
       ('user3', 'ROLE_USER');

INSERT INTO employee (birthday, phone, start_work, full_name, city, username)
VALUES (DATE '2002-12-28', '+79992434271', DATE '2018-10-10', 'Zubenko Mikhail Petrovich1', 'London', 'user1'),
       (DATE '2002-12-26', '+79992434271', DATE '2018-10-10', 'Zubenko Mikhail Petrovich2', 'London', 'user2'),
       (DATE '2002-12-24', '+79992434271', DATE '2018-10-10', 'Zubenko Mikhail Petrovich3', 'London', 'user3');

INSERT INTO project (name, description)
VALUES ('Just Tinkoff1', 'Just description1'),
       ('Just Tinkoff2', 'Just description2');

INSERT INTO employee_project_relationship (employee_id, project_id)
VALUES (1, 1),
       (2, 1),
       (3, 1);

INSERT INTO present (name, description, link, employee_id)
VALUES ('Present1', 'Description1', 'link1', 1),
       ('Present2', 'Description2', 'link2', 1),
       ('Present3', 'Description3', 'link3', 1),
       ('Present4', 'Description4', 'link4', 2),
       ('Present5', 'Description5', 'link5', 2),
       ('Present6', 'Description6', 'link6', 3);